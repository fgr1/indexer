#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <math.h>


#define BUFFER 2000
#define MAX_WORD_LEN 50
#define MAX_NUM_WORDS 300000

typedef struct word_count {
    char word[MAX_WORD_LEN];
    int count;
    struct word_count *next;
} word_count;

typedef struct hash_table {
    word_count *table[MAX_NUM_WORDS];
    int num_words;
} hash_table;

typedef struct {
    char term[BUFFER];
    double tfidf;
} file_score;

void cria_tabela(char *filename, hash_table *ht);
void ordena_tabela(hash_table *ht);
void freq_function(hash_table *ht, int n);
void freq_word_function(char *filename, char *word, hash_table *ht);
int compare_file_scores(const void *a, const void *b);
void search_function(int num_arquivos, char **arquivos, char **termos, int num_search_terms, hash_table *ht);
void processWord(char* word);
void add_word(hash_table *ht, char *word);
int separar_argumentos_arquivo(char *str, char ***arquivos);


int main(int argc, char *argv[]) {
    if (argc < 4) {
        printf("Modo de uso: ./program <funcao> <complemento> <arquivo1> [<arquivo2> ...]\n");
        return 1;
    }

    char *function = argv[1];
    char *complement = argv[2];
    char *filename = argv[3];
    int freq_n = 0;
    char *freq_word = NULL;
    char **search_term = NULL;
    int num_search_terms = 0;

    if (strcmp(function, "--freq") == 0) {
        freq_n = atoi(complement);
    } else if (strcmp(function, "--freq-word") == 0) {
        freq_word = complement;
        processWord(freq_word);
    } else if (strcmp(function, "--search") == 0) {
        int search_term_size = 1;
        search_term = malloc(sizeof(char *) * search_term_size);

        for (int i = 3; i < argc; i++) {
            if (access(argv[i], F_OK) != -1) {
                break;
            } else {
                search_term[num_search_terms] = strdup(argv[i]);
                num_search_terms++;

                if (num_search_terms >= search_term_size) {
                    search_term_size *= 2;
                    search_term = realloc(search_term, sizeof(char *) * search_term_size);
                }
            }
        }
    } else {
        printf("Erro: funcao invalida '%s'\n", function);
        return 1;
    }

    hash_table ht;
    ht.num_words = 0;

    if (strcmp(function, "--freq") == 0) {
        printf("Frequencia das %d palavras mais comuns em %s:\n", freq_n, filename);
        cria_tabela(filename, &ht);
        ordena_tabela(&ht);
        freq_function(&ht, freq_n);
    } else if (strcmp(function, "--freq-word") == 0) {
        printf("Frequencia da palavra \"%s\" em %s:\n", freq_word, filename);
        freq_word_function(filename, freq_word, &ht);
    } else if (strcmp(function, "--search") == 0) {
        char **arquivos = malloc(sizeof(char*) * argc);
        int num_arquivos = 0;
        for (int i = 3; i < argc; i++) {
            if (access(argv[i], F_OK) != -1) {
                arquivos[num_arquivos] = argv[i];
                num_arquivos++;
            }
    }
    search_function(num_arquivos, arquivos, search_term, num_search_terms, &ht);
    free(arquivos);
    }
    free(search_term);
    for (int i = 0; i < ht.num_words; i++) {
        free(ht.table[i]);
    }

    return 0;
}
//=====================================================FUNCOES=======================================================//

void cria_tabela(char *filename, hash_table *ht){

    char *arqword;
    char buffer[BUFFER];

    FILE *file = fopen(filename, "r");
    if (!file) {
        printf("Erro: nao foi possivel abrir o arquivo: '%s'\n", filename);
        return;
    }

    while (fgets(buffer, sizeof(buffer), file) != NULL) {
        arqword = strtok(buffer, " ");
        while (arqword != NULL) {
            processWord(arqword);
            add_word(ht, arqword);
            arqword = strtok(NULL, " ");
        }
    }
    fclose(file);
}

void ordena_tabela(hash_table *ht){
    for (int i = 0; i < ht->num_words - 1; i++) {
        for (int j = i + 1; j < ht->num_words; j++) {
            if (ht->table[i]->count < ht->table[j]->count) {
                word_count *temp = ht->table[i];
                ht->table[i] = ht->table[j];
                ht->table[j] = temp;
            }
        }
    }
}

void freq_function(hash_table *ht, int n) {

    for (int i = 0; i < n && i < ht->num_words; i++) {
        printf("%d - %s: %d\n", i+1, ht->table[i]->word, ht->table[i]->count);
    }
}
//------------------------------------------------------------------------------------------------------------------//

void freq_word_function(char *filename, char *word, hash_table *ht) {

    char *arqword;
    char buffer[BUFFER];

    FILE *file = fopen(filename, "r");
    if (!file) {
        printf("Erro: nao foi possivel abrir o arquivo: '%s'\n", filename);
        return;
    }
    while (fgets(buffer, sizeof(buffer), file) != NULL) {
        arqword = strtok(buffer, " ");
        while (arqword != NULL) {
            processWord(arqword);
            if (strcmp(arqword, word) == 0) {
                add_word(ht, arqword);
            }
            arqword = strtok(NULL, " ");
        }
    }
    // print the frequency of the word
    if (ht->num_words > 0) {
        printf("A palavra \"%s\" aparece %d vezes no arquivo.\n", word, ht->table[0]->count);
    } else {
        printf("A palavra \"%s\" nao aparece no arquivo.\n", word);
    }
    fclose(file);
}
//------------------------------------------------------------------------------------------------------------------//

int compare_file_scores(const void *a, const void *b) {
    file_score *fa = (file_score*) a;
    file_score *fb = (file_score*) b;
    return (fa->tfidf < fb->tfidf) - (fa->tfidf > fb->tfidf);
}
//------------------------------------------------------------------------------------------------------------------//

void search_function(int num_arquivos, char **arquivos, char **termos, int num_search_terms, hash_table *ht) {
    int i, j, k, word_count, document_count;
    char buffer[BUFFER];
    FILE *file;

    // Contagem de documentos que contém cada termo
    int doc_count[num_search_terms];
    memset(doc_count, 0, num_search_terms * sizeof(int));
    for (i = 0; i < num_search_terms; i++) {
        for (j = 0; j < num_arquivos; j++) {
            if (ht->table[i][j].count > 0) {
                doc_count[i]++;
            }
        }
    }

    // Cálculo de TF-IDF para cada arquivo
    file_score scores[num_arquivos][num_search_terms];
    for (i = 0; i < num_arquivos; i++) {
        word_count = 0;
        char *buffer = (char*) malloc(BUFFER * sizeof(char));
        file = fopen(arquivos[i], "r");

        while (fgets(buffer, BUFFER, file)) {
            char *token = strtok(buffer, " \t\n");
            while (token != NULL) {
                for (j = 0; j < num_search_terms; j++) {
                    if (strcmp(token, termos[j]) == 0) {
                        scores[i][j].tfidf += (double) ht->table[j][i].count / (double) word_count * log((double) num_arquivos / (double) doc_count[j]);
                    }
                }
                word_count++;
                token = strtok(NULL, " \t\n");
            }
        }

        fclose(file);
        free(buffer);
    }

    // Ordenação dos arquivos por relevância decrescente
    file_score all_scores[num_arquivos * num_search_terms];
    for (i = 0; i < num_arquivos; i++) {
        for (j = 0; j < num_search_terms; j++) {
            all_scores[i * num_search_terms + j] = scores[i][j];
        }
    }
    qsort(all_scores, num_arquivos * num_search_terms, sizeof(file_score), compare_file_scores);

    // Exibição da lista de arquivos por relevância decrescente
    printf("Resultados da busca:\n");
    printf("--------------------\n");
    for (i = 0; i < num_arquivos * num_search_terms; i++) {
        if (all_scores[i].tfidf > 0) {
            printf("%s: %f\n", all_scores[i].term, all_scores[i].tfidf);
        }
    }
}
//------------------------------------------------------------------------------------------------------------------//

void processWord(char* word) {
    int i, j;
    for (i = 0, j = 0; word[i]; i++) {
        if (isalpha(word[i])) {
            word[j++] = tolower(word[i]);
        }
    }
    word[j] = '\0';
}
//------------------------------------------------------------------------------------------------------------------//

void add_word(hash_table *ht, char *word) {

    // processa a palavra
    processWord(word);

    // verifica se a palavra foi processada para uma string vazia
    if (strlen(word) <= 2) {
        return;
    }

    // procura pela palavra na tabela hash
    int index = -1;
    for (int i = 0; i < ht->num_words; i++) {
        if (strcmp(ht->table[i]->word, word) == 0) {
            index = i;
            break;
        }
    }

    // se a palavra já existe na tabela, incrementa a contagem
    if (index >= 0) {
        ht->table[index]->count++;
    }
    // caso contrário, adiciona a palavra à tabela
    else {
        word_count *wc = malloc(sizeof(word_count));
        strncpy(wc->word, word, MAX_WORD_LEN);
        wc->count = 1;
        ht->table[ht->num_words] = wc;
        ht->num_words++;
    }
}
